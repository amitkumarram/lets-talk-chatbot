import datetime
import json
import logging
import smtplib
import ssl
import threading
import time
from email.message import EmailMessage
import os
import jwt
from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage

from fileinput import filename

ALLOWED_EXTENSIONS = ["png", "jpg", "jpeg", "gif", "svg"]
from flask import current_app, jsonify, request
from functools import wraps

success = "SUCCESS"
error = "ERROR"
active = "ACTIVE"
inactive = "INACTIVE"
delete = "DELETE"


def user_Access(U):
    @wraps(U)
    def wrapper(*args, **kwargs):
        token = request.headers.get('token')
        # print(token)
        try:
            decoded_token = jwt.decode(token, current_app.config['SECRET_KEY'], algorithms=['HS256'])
            print(decoded_token)
            if decoded_token.get("role_id") == '1':
                print(decoded_token)
                return U(*args, **kwargs)
            else:
                return jsonify(
                    {"message": "USER AUTHORISATION REQUIRED or Signature Expired", "status": "val_error"}), 401
        except Exception as e:
            import traceback
            traceback.print_exc()
            return jsonify({"message": "USER AUTHORISATION REQUIRED or Signature Expired", "status": "val_error"}), 401

    return wrapper


def uuser_Access(U):
    @wraps(U)
    def wrapper(*args, **kwargs):
        token = request.headers.get('token')
        # print(token)
        try:
            decoded_token = jwt.decode(token, current_app.config['SECRET_KEY'], algorithms=['HS256'])
            print(decoded_token)
            if decoded_token.get("role_id") == '3' or decoded_token.get("role_id") == '2' or decoded_token.get(
                    "role_id") == '1':
                print(decoded_token)
                return U(*args, **kwargs)
            else:
                return jsonify(
                    {"message": "USER AUTHORISATION REQUIRED or Signature Expired", "status": "val_error"}), 401
        except Exception as e:
            import traceback
            traceback.print_exc()
            return jsonify({"message": "USER AUTHORISATION REQUIRED or Signature Expired", "status": "val_error"}), 401

    return wrapper


# def duser_Access(U):
#     @wraps(U)
#     def wrapper(*args, **kwargs):
#         token = request.headers.get('token')
#         #print(token)
#         try:
#             decoded_token = jwt.decode(token, current_app.config['SECRET_KEY'], algorithms=['HS256'])
#             print(decoded_token)
#             if decoded_token.get("role_id") =='2':
#                 print(decoded_token)
#                 return U(*args, **kwargs)
#             else:
#                 return jsonify({"message": "USER AUTHORISATION REQUIRED or Signature Expired", "status": "val_error"}), 401
#         except Exception as e:
#             import traceback
#             traceback.print_exc()
#             return jsonify({"message": "USER AUTHORISATION REQUIRED or Signature Expired", "status": "val_error"}), 401

#     return wrapper

def email_notification(report):
    try:
        msg = EmailMessage()
        msg.set_content(json.dumps(report))
        msg["Subject"] = " NEWS Alert "
        msg["From"] = "xlrt.prod.news@gmail.com"
        msg["To"] = "shakya.dutta@ivanwebsolutions.com"
        context = ssl.create_default_context()
        with smtplib.SMTP("smtp.gmail.com", port=587) as smtp:
            smtp.starttls(context=context)
            smtp.login(msg["From"], "ymzs epbb hbqk sxij")
            smtp.send_message(msg)
        response = {"status": 'success', "message": 'message sent through email'}
        return response
    except Exception as e:
        response = {"status": 'error', "message": f'{str(e)}'}
        return response


def event(event_name):
    def decorator(method):
        def timer(*args, **kwargs):
            _event_name = event_name.replace(" ", "_")
            logger = logging.getLogger(_event_name)
            ex = None
            data = None
            result = "SUCCESS"
            begin = time.time()
            try:
                data = method(*args, **kwargs)
            except Exception as e:
                ex = e
                result = "FAILURE"
            end = time.time()
            response = json.dumps(
                {
                    "eventName": _event_name,
                    "status": result,
                    "eventTime": end - begin,
                    "metaData": {
                        "thread": threading.current_thread().ident,
                        "class": method.__class__.__name__,
                        "method": method.__name__,
                    },
                },
            )
            if ex:
                logger.error(response)
                raise ex
            logger.info(response)
            return data

        return timer

    return decorator


# ALLOWED_EXTENSIONS = ["png", "jpg", "jpeg", "gif", "svg"]


# def save_image(image: FileStorage, folder: str = None) -> str:
#     if image and image.filename.split(".")[-1].lower() in ALLOWED_EXTENSIONS:
#         filename = secure_filename(image.filename)
#         image.save(os.path.join("app/UserProfile/uploads/" + folder, filename))
#         return "uploads/" + folder + filename


def save_file(file, filename, user_id):
    try:
        # Find out what the year, month, day and time is
        now = datetime.datetime.now()

        # Add the time and the .txt extension as the filename
        filename = filename

        path = f"static/pics/{user_id}/"

        # Check if the path exists. If it doesn't create the directories
        if not os.path.exists(path):
            os.makedirs(path)

        # Create the file
        file.save(os.path.join(path, filename))

        response = {"status": 'Success', "message": path + filename}
        print(response)
        return response
    except Exception as e:
        response = {"status": 'Error', "message": f'{str(e)}'}
        print(response)
        return jsonify(response), 200


class GenerateApiToken:

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret

    def generate_token(self, client_id, client_secret):
        try:
            if self.client_id == client_id and self.client_secret == client_secret:

                payload = {
                    'id': self.client_id,  # client_id
                    'secret': self.client_secret,  # client_secret
                    # expire time of the jwt tokens (datetime.timedelta(days=5) expires in 5 days)
                    'exp': datetime.datetime.utcnow() + datetime.timedelta(days=5),
                }
                # encoding the JWT Token
                jwt_token = {'Access_token': jwt.encode(payload, "SECRET", algorithm='HS256'), 'success': True}
                response = {"status": 'success', "message": 'JWT token created successfully', 'token': jwt_token}
                return response
            else:
                response = {"status": 'error', "message": 'JWT token created unsuccessfully due to invalid '
                                                          'client_secret or client_id'}
                return response

        except Exception as e:
            response = {"status": 'error', "message": f'{str(e)}'}
            return response


class ValidateApiToken:

    def __init__(self, headers, client_id, client_secret):

        self.client_id = client_id  # client_id
        self.client_secret = client_secret  # client_secret
        self.headers = headers

    def verify_token(self):

        try:
            # decoding the JWT Token
            decoded_token = jwt.decode(self.headers, 'SECRET', algorithms=['HS256'])
            # To verify data encode in GenerateApiToken == data encode in ValidateApiToken
            print(decoded_token)
            if decoded_token['id'] == self.client_id and decoded_token['secret'] == self.client_secret:
                return {"status": 'success', 'message': 'Access Granted'}
            else:
                return {"status": 'error', 'message': 'Access denied due invalid credentials'}
        except jwt.ExpiredSignatureError as e:
            response = {"status": 'error', "message": f'{str(e)}'}
            return response


def email_notification(report, email):
    try:
        msg = EmailMessage()
        msg.set_content(json.dumps(report))
        msg["Subject"] = "Employee"
        msg["From"] = "xlrt.prod.news@gmail.com"
        msg["To"] = email
        context = ssl.create_default_context()
        with smtplib.SMTP("smtp.gmail.com", port=587) as smtp:
            smtp.starttls(context=context)
            smtp.login(msg["From"], "ymzs epbb hbqk sxij")
            smtp.send_message(msg)
        response = {"status": 'success', "message": 'message sent through email'}
        return response
    except Exception as e:
        response = {"status": 'error', "message": f'{str(e)}'}
        return response
