# Importing the relevant libraries
import websockets
import asyncio
import threading
import jwt
import json
import time
from app import create_app

import ThreadPoolExecutorPlus
from ThreadPoolExecutorPlus import ThreadPoolExecutor

# Server data
# PORT = 8080
# HOST = "127.0.0.1"
# PORT = 60500
# HOST = "192.168.20.117"
PORT = 8080
HOST = "103.13.242.134"

print(f"Server listening on Host {HOST} Port " + str(PORT))

def main():
    # Start the server
    start_server = websockets.serve(create_app, HOST, PORT, close_timeout=5)
    asyncio.get_event_loop().run_until_complete(start_server)
    try:
        asyncio.get_event_loop().run_forever()
    finally:
        asyncio.get_event_loop().run_until_complete(asyncio.get_event_loop().shutdown_asyncgens())
        asyncio.get_event_loop().close()



if __name__ =='__main__':
  with ThreadPoolExecutor() as executor:
      executor.set_daemon_opts(min_workers = 2 , max_workers = 10 , keep_alive_time = 10)
      print('ThreadPoolExecutor is rounning')
      for _ in range(10):
          executor.submit(main())
          time.sleep(0.01)
          executor.shutdown(wait=True)