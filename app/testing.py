import time
import pprint
import json

# from app.db_config import database_connect_mongo
# from app.ChatBotModels import chatbotObj
from websocket import create_connection

convert = lambda a,d: json.loads(d) if a=="str" else json.dumps(d) if a=="dict" else None

# ws = create_connection("ws://184.171.249.242:8080")
ws = create_connection("ws://localhost:8080")

tokenData = {'token':'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcl9ubyI6IjE5OTU3MiIsImVtYWlsX2lkIjoiNTBAeW9wbWFpbC5jb20iLCJyb2xlX2lkIjoiMyIsIl9pZCI6IjYyZjFmMjU3YWQ4Y2NmYWY5NDA1NzZiOCIsInBob25lX251bWJlciI6IjYxLTg4ODg4ODg4ODgiLCJ1c2VyX2lkIjoiNjJmMWYyNTdhZDhjY2ZhZjk0MDU3NmI4IiwiZXhwIjoxNjYzODMxMDk5LCJtb2JpbGUiOiI2MS04ODg4ODg4ODg4In0.UzqI7B7wYpRWYL_2CTBWWPktoXv1UuZoo__jMpczsfw'}

ws.send(convert("dict",tokenData))
chatData = convert("str",ws.recv())
print(chatData)
chatId = chatData['data']['chatid']


time.sleep(1)
print('====================================================')
nameSet = "Hi"
print(f'Stap 1 === {nameSet}')
ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
pprint.pprint(convert("str",ws.recv()))
print()


time.sleep(1)
print('====================================================')
nameSet = "next&2&"
print(f'Stap 2 === {nameSet}')
ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
pprint.pprint(convert("str",ws.recv()))
print()


time.sleep(1)
print('====================================================')
nameSet = "I am suffering from SIDE EFFECTS"
print(f'Stap 3 === {nameSet}')
ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
pprint.pprint(convert("str",ws.recv()))
print()


time.sleep(1)
print('====================================================')
nameSet = "next&5&"
print(f'Stap 4 === {nameSet}')
ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
pprint.pprint(convert("str",ws.recv()))
print()


time.sleep(1)
print('====================================================')
nameSet = "Existing SIDE EFFECTS"
print(f'Stap 5 === {nameSet}')
ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
pprint.pprint(convert("str",ws.recv()))
print()


time.sleep(1)
print('====================================================')
nameSet = "next&9&"
print(f'Stap 5 1 === {nameSet}')
ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
pprint.pprint(convert("str",ws.recv()))
print()


time.sleep(1)
print('====================================================')
nameSet = "diarrhoea"
print(f'Stap 5 2 === {nameSet}')
ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
pprint.pprint(convert("str",ws.recv()))
print()


time.sleep(1)
print('====================================================')
nameSet = "next&12&"
print(f'Stap 5 3 === {nameSet}')
ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
pprint.pprint(convert("str",ws.recv()))
print()


time.sleep(1)
print('====================================================')
nameSet = "Within last week"
print(f'Stap 5 4 === {nameSet}')
ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
pprint.pprint(convert("str",ws.recv()))
print()


time.sleep(1)
print('====================================================')
nameSet = "&diarrhoea&"
print(f'Stap 5 5 === {nameSet}')
ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
pprint.pprint(convert("str",ws.recv()))
print()


time.sleep(1)
print('====================================================')
nameSet = """1) Increase of < 4 motions/day over baseline or mild increase in stoma output compared to baseline"""
print(f'Stap 5 6 === {nameSet}')
ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
pprint.pprint(convert("str",ws.recv()))
print()


# time.sleep(1)
# print('====================================================')
# nameSet = """idle_stop"""
# print(f'Stap 5 7 === {nameSet}')
# ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
# pprint.pprint(convert("str",ws.recv()))
# print()


# time.sleep(1)
# print('====================================================')
# nameSet = f"""{chatId}"""
# print(f'Stap 5 7 === {nameSet}')
# ws.send(convert("dict",{'user':chatId,'chatid':chatId}))
# pprint.pprint(convert("str",ws.recv()))
# print()


time.sleep(1)
print('====================================================')
nameSet = "next&16&"
print(f'Stap 5 7 === {nameSet}')
ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
pprint.pprint(convert("str",ws.recv()))
print()


time.sleep(1)
print('====================================================')
nameSet = """b) I'm up and about > 50% of waking hours."""
print(f'Stap 5 8 === {nameSet}')
ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
pprint.pprint(convert("str",ws.recv()))
print()


time.sleep(1)
print('====================================================')
nameSet = "next&21&"
print(f'Stap 5 9 === {nameSet}')
ws.send(convert("dict",{'user':nameSet,'chatid':chatId}))
pprint.pprint(convert("str",ws.recv()))
print()



# print()
# while True:
# 	arg = input("Text: ")
# 	if arg == "stop":
# 		break
	
	
# 	dataSet = {"user":arg,"chatid":chatId}
# 	print(dataSet)
# 	print()
# 	ws.send(json.dumps(dataSet))
# 	chatData
# 	print(ws.recv())
# 	optionCheck = chatData['data']['option']
	

# obj = chatbotObj()
# data = {'token':'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcl9ubyI6IjE5OTU3MiIsImVtYWlsX2lkIjoiNTBAeW9wbWFpbC5jb20iLCJyb2xlX2lkIjoiMyIsIl9pZCI6IjYyZjFmMjU3YWQ4Y2NmYWY5NDA1NzZiOCIsInBob25lX251bWJlciI6IjYxLTg4ODg4ODg4ODgiLCJ1c2VyX2lkIjoiNjJmMWYyNTdhZDhjY2ZhZjk0MDU3NmI4IiwiZXhwIjoxNjYzODMxMDk5LCJtb2JpbGUiOiI2MS04ODg4ODg4ODg4In0.UzqI7B7wYpRWYL_2CTBWWPktoXv1UuZoo__jMpczsfw'}
# obj.main(data)
# print(obj.response())
# print()
# print(obj.roomDetails)
# print()
# print(len(obj.roomDetails))



# time.sleep(1)
# print('====================================================')
# print('Stap 1 === Hi')
# obj.main({'user':'Hi','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()


# time.sleep(1)
# print('====================================================')
# print('Stap 2 === next&2&')
# obj.main({'user':'next&2&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 3 === I am suffering from SIDE EFFECTS')
# obj.main({'user':'I am suffering from SIDE EFFECTS','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 4 === next&5&')
# obj.main({'user':'next&5&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 5 === Previous SIDE EFFECTS')
# obj.main({'user':'Previous SIDE EFFECTS','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 5 === Existing SIDE EFFECTS')
# obj.main({'user':'Existing SIDE EFFECTS','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()



# time.sleep(1)
# print('====================================================')
# print('Stap 6 === next&10&')
# obj.main({'user':'next&10&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 6 === next&27&')
# obj.main({'user':'next&27&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 7 === vision')
# obj.main({'user':'vision','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()



# time.sleep(1)
# print('====================================================')
# print('Stap 5 1 === Existing SIDE EFFECTS')
# obj.main({'user':'Existing SIDE EFFECTS','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 5 2 === next&9&')
# obj.main({'user':'next&9&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 5 3 === headache')
# obj.main({'user':'headache','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 6 1 === Previous SIDE EFFECTS')
# obj.main({'user':'Previous SIDE EFFECTS','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 6 2 === next&10&')
# obj.main({'user':'next&10&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()



# time.sleep(1)
# print('====================================================')
# print('Stap 6 2 === next&21&')
# obj.main({'user':'next&21&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 6 3 === Absence of data')
# obj.main({'user':'Absence of data','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()

#==============================Exist side effect start==========================

# time.sleep(1)
# print('====================================================')
# print('Stap 1 === NEW SIDE EFFECT')
# obj.main({'user':'NEW SIDE EFFECT','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 2 === next&11&')
# obj.main({'user':'next&11&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 3 === Diarrhoea')
# obj.main({'user':'Diarrhoea','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 4 === next&12&')
# obj.main({'user':'next&12&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 5 === Within last 48 hrs')
# obj.main({'user':'Within last 48 hrs','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 6 === &Diarrhoea&')
# obj.main({'user':'&Diarrhoea&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()
# print(obj.roomDetails)
# print()


# time.sleep(1)
# print('====================================================')
# print('Stap 7 === 1) Increase of < 4 motions/day over baseline or mild increase in stoma output compared to baseline')
# obj.main({'user':'1) Increase of < 4 motions/day over baseline or mild increase in stoma output compared to baseline','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 8 === next&16&')
# obj.main({'user':'next&16&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print("Stap 9 === a) Im up and about > 50% of waking hours.")
# obj.main({'user':"a) Im up and about > 50% of waking hours.",'chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print("Stap 10 === next&21&")
# obj.main({'user':"next&21&",'chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

#==============================Exist side effect end============================

#==============================New side effect start============================

# time.sleep(1)
# print('====================================================')
# print('Stap 1 === NEW SIDE EFFECT')
# obj.main({'user':'NEW SIDE EFFECT','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 2 === next&11&')
# obj.main({'user':'next&11&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 3 === Vision')
# obj.main({'user':'Vision','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 4 === next&12&')
# obj.main({'user':'next&12&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 5 === Within last 48 hrs')
# obj.main({'user':'Within last 48 hrs','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 6 === &Vision&')
# obj.main({'user':'&Vision&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()
# print(obj.roomDetails)
# print()

#==============================New side effect end==============================

#=======================Exist Previous SIDE EFFECTS start=======================

# time.sleep(1)
# print('====================================================')
# print('Stap 1 === Previous SIDE EFFECTS')
# obj.main({'user':'Previous SIDE EFFECTS','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 2 === next&10&')
# obj.main({'user':'next&10&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 3 === Diarrhoea')
# obj.main({'user':'Diarrhoea','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 4 === next&12&')
# obj.main({'user':'next&12&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 5 === Within last 48 hrs')
# obj.main({'user':'Within last 48 hrs','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 6 === &Diarrhoea&')
# obj.main({'user':'&Diarrhoea&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()


# time.sleep(1)
# print('====================================================')
# print('Stap 7 === 1) Increase of < 4 motions/day over baseline or mild increase in stoma output compared to baseline')
# obj.main({'user':'1) Increase of < 4 motions/day over baseline or mild increase in stoma output compared to baseline','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 8 === next&16&')
# obj.main({'user':'next&16&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print("Stap 9 === a) Im up and about > 50% of waking hours.")
# obj.main({'user':"a) Im up and about > 50% of waking hours.",'chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print("Stap 10 === next&21&")
# obj.main({'user':"next&21&",'chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()


#=======================Exist Previous SIDE EFFECTS end=========================

#=======================Exist Existing SIDE EFFECTS start=======================

# time.sleep(1)
# print('====================================================')
# print('Stap 1 === Existing SIDE EFFECTS')
# obj.main({'user':'Existing SIDE EFFECTS','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 2 === next&9&')
# obj.main({'user':'next&9&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 3 === Vomiting')
# obj.main({'user':'Vomiting','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 4 === next&12&')
# obj.main({'user':'next&12&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 5 === Within last 48 hrs')
# obj.main({'user':'Within last 48 hrs','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 6 === &vomiting&')
# obj.main({'user':'&vomiting&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 7 === 1) Increase of < 4 motions/day over baseline or mild increase in stoma output compared to baseline')
# obj.main({'user':'1) Increase of < 4 motions/day over baseline or mild increase in stoma output compared to baseline','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print('Stap 8 === next&16&')
# obj.main({'user':'next&16&','chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print("Stap 9 === a) I'm up and about > 50% of waking hours.")
# obj.main({'user':"a) I'm up and about > 50% of waking hours.",'chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()

# time.sleep(1)
# print('====================================================')
# print("Stap 9 === next&21&")
# obj.main({'user':"next&21&",'chatid':'242375&2022&07&22&15&03&04'})
# pprint.pprint(obj.response())
# print()
# print(obj.chatTemp)
# print()


#=======================Exist Existing SIDE EFFECTS end=======================



