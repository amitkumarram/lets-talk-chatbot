from app.db_config import database_connect_mongo
from app.ChatBotModels import chatbotObj
from app.db_chatHist import db_hist

import datetime as dt
import jwt
import json

from flask import Flask, render_template
from flask_sock import Sock


objChat = chatbotObj()
objdb_hist = db_hist()



app = Flask(__name__)
sock = Sock(app)
app.config['SOCK_SERVER_OPTIONS'] = {'ping_interval': 10}


@sock.route('/')
def echo(websocket):
    while True:
        message = websocket.receive()
        #=============================================
        message = json.loads(message)
        dateTime = str(dt.datetime.now())
        dateTime = dateTime[:dateTime.rfind('.')]
        print('=============Chatbot=========')
        print(dateTime,message)
        print(type(message))
        if type(message)==dict:
            objChat.main(message)
            print('---------------------')
            # print(objChat.roomDetails)
            objResponse = objChat.response()

            if "chatid" in list(message.keys()):
                data = {'chatId': message['chatid'], 'response': "chatbot", 'text':objResponse}
                objdb_hist.chat_history(action="update", data=data, status="ACTIVE")

            objResponse['datetime'] = dateTime

            print(dateTime,objResponse)
            print('---------------------')
            print(len(objChat.roomDetails))
            # await asyncio.sleep(0.10)
            websocket.send(json.dumps(objResponse))
            
            # await asyncio.sleep(5)
            # await websocket.send("its working")
        else:
            websocket.send("str value")
        


        # ws.send(data[::-1])