import requests
import json

from app.db_chatHist import db_hist

objDB = db_hist()


class userDetails:
    def __init__(self, urls=None):
        self.url = urls
        self.headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        
    def vitals(self, data=None, token=None):
        print("=========Vitals========")

        temp_url = self.url + '/common/vitals/details'
        temp_headers = self.headers
        temp_headers['token'] = token
        # data['user_id'] = "62f1f257ad8ccfaf940576b8"
        vitals_list = requests.post(temp_url, data=json.dumps(data), headers=temp_headers)
        vitals_list = vitals_list.json()
        # print(vitals_list)
        # print()
        
        vitals_list = vitals_list if len(vitals_list['data'])!=0  else {'data':[{}]}
        vitalText = ""
        
        countV = 0
        breakT = 5
        if vitals_list['data'][0] != {}:
            for x in vitals_list['data']:
                for y in x['value']:
                    for z in y['vital']:
                        vitalStr = f"<br>{z['title']}:&nbsp;<b>{z['value']}&nbsp;{z['unit']}</b><br>"
                        vitalText += vitalStr
                        countV += 1
                        if countV == breakT:
                            break
                    if countV == breakT:
                        break
                if countV == breakT:
                    break
                        
                        
        return vitalText if vitalText != "" else "As per our records your current Health vitals are not updated. Please add your Vitals"

    def sideEffects(self, data=None, token=None, req=None):
        temp_url = self.url + '/common/usersideeffectsview'
        temp_headers = self.headers
        temp_headers['token'] = token
        siteEff_list = requests.post(temp_url, data=json.dumps(data), headers=temp_headers)
        siteEff_list = siteEff_list.json()
        
        siteEff_list = siteEff_list if siteEff_list['data']!=[] else {'data':[{}]}
        sideEffect = []


        try:
            if siteEff_list['data'][0] != {}:
                for x in siteEff_list['data']:
                    if x['status']==req: # ACTIVE or INACTIVE
                        temp_dict = {"action_type":"checkbox","setValue":None}
                        temp_dict['name'] = x['side_effect_name']
                        sideEffect.append(temp_dict)
                        
            if len(sideEffect) == 0:
                temp_dict = {"action_type":"checkbox","setValue":None,"name":"Absence of data"}
                sideEffect.append(temp_dict)

        except Exception as e:
            print('ErrorSEF ',str(e))
            pass


        return sideEffect

    def sideEffectAdd(self, data=None, token=None):
        #{'message': 'User Side Effects added successfully', 'status': 'success'}
        #{'message': 'User Side Effects already exists', 'status': 'success'}
        temp_url = self.url + '/admin/usersideeffectadd'
        temp_url_edit = self.url + '/admin/sideeffectsaddedit'
        statusChange_url = self.url + '/admin/usersideeffectsstatus'

        dataUserSideEffect = {"order":"select","data":{"table":"side_effects","condition":{}}}
        UserSideEffect = objDB.database(dataUserSideEffect)
        

        related_sideEffect = {}
        if UserSideEffect['status_code']==200 and len(UserSideEffect['data'])!=0:
            for x in UserSideEffect['data']:
                related_sideEffect[x['name']] = x['_id']
        else:
            related_sideEffect = {
                                  "headache":"side_00002",
                                  "vomiting":"side_00003",
                                  "bradycardia":"side_00006",
                                  "constipation":"side_00007",
                                  "vision":"side_00053",
                                  "Diarrhoea":"side_00009",
                                  "increased weight":"side_00034",
                                  "nausea/vomitting":"side_00024",
                                  "weight gain":"side_00010",
                                  "ecg- qt prolonged":"side_00054",
                                  "dysphagia":"side_00008"
                             }

        

        temp_headers = self.headers
        temp_headers['token'] = token
        # print("sideEffect==1")
        dataAdd = {x:y for x,y in related_sideEffect.items() if str(x.lower()).replace(' ','_')==data['side_id'].lower().replace(' ','_')}
        siteEff_list = {'message': 'User Side Effects added successfully', 'status': 'success'}
        # print("sideEffect==2")

        try:
            # print("sideEffect==3")
            if dataAdd == {}:
                dataAdd = {
                        "side_name":f"{data['side_id'].lower()}",
                        "side_id":"",
                        "med_id":"med_006",
                        "synonyms":""
                       }
                # print("sideEffect==4")
                # print(dataAdd)
                siteEff_list_add = requests.post(temp_url_edit, data=json.dumps(dataAdd), headers=temp_headers)
                siteEff_list_add = siteEff_list_add.json()
                # print(siteEff_list_add)
                # print("sideEffect==5")
                if str(siteEff_list_add['message']).lower() == 'side effect added successfully' or siteEff_list_add['status'] == 'success':
                    data['side_id'] = siteEff_list_add['data']['_id']
                    print(siteEff_list)
                    siteEff_list = requests.post(temp_url, data=json.dumps(data), headers=temp_headers)
                    print(siteEff_list)
                # print("sideEffect==6")
                    
                
            else:
                # print("sideEffect==7")
                data['side_id'] = list(dataAdd.values())[0]
                siteEff_list = requests.post(temp_url, data=json.dumps(data), headers=temp_headers)
                siteEff_list = siteEff_list.json()
                # print("sideEffect==8")
                IdUserSideEffect = {"order":"find","data":{"table":"user_side_effects","condition":{"user_id":data['user_id'],"side_effect_id":data['side_id']}}}
                GetIdUserSideEffect = objDB.database(IdUserSideEffect)
                if GetIdUserSideEffect["data"]:
                    dataSet = {"user_side_id":GetIdUserSideEffect["data"]["_id"],"status":"ACTIVE"}
                    statusChange = requests.post(statusChange_url, data=json.dumps(dataSet), headers=temp_headers)#, headers=temp_headers
                    print(statusChange.json())
                
                
                
        except Exception as e:
            # print("sideEffect==9")
            print('sideEffect_error',str(e))
            

        return None

    
    def showAllreq(self, text=None):
        dataUserFind = {"order":"select","data":{"table":"tbl_chatbot_1","condition":{}}}
        getUserRes = objDB.database(dataUserFind)
        getRequestText = [x['request_text'].lower().replace(' ','_') for x in getUserRes['data'] if x]
        text = [x['name'] for x in text if '&'+x['name'].lower().replace(' ','_')+'&' in getRequestText]
        reqCheck = False
        
        if len(text)!=0:
            reqCheck = True
        else:
            reqCheck = False
        
        return reqCheck




# url = "http://letstalk.dev13.ivantechnology.in/common/vitals/details"

# url = "http://letstalk.dev13.ivantechnology.in"
# token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcl9ubyI6IjE5OTU3MiIsImVtYWlsX2lkIjoiNTBAeW9wbWFpbC5jb20iLCJyb2xlX2lkIjoiMyIsIl9pZCI6IjYyZjFmMjU3YWQ4Y2NmYWY5NDA1NzZiOCIsInBob25lX251bWJlciI6IjYxLTg4ODg4ODg4ODgiLCJ1c2VyX2lkIjoiNjJmMWYyNTdhZDhjY2ZhZjk0MDU3NmI4IiwiZXhwIjoxNjYxODQxODIzLCJtb2JpbGUiOiI2MS04ODg4ODg4ODg4In0.kyYDQNlDLpKLDsjH0IP3AJFWJzPhE0iSiQyprv9Ms3Q"
# data = {"user_id":"62f1f257ad8ccfaf940576b8"}


# obj = userDetails(urls=url)
# obj.vitals(token=token, data=data)

# obj.sideEffects(token=token, data=data, req='ACTIVE')

# Here are your Existing SIDE EFFECTS
# existSideEffect


# &Dysphagia&
# &Diarrhoea&

# &Diarrhoea

# ['headache', 'dizziness', 'vomiting', 'bradycardia', 'constipation', 
# 'vomiting', 'headache', 'Diarrhoea']