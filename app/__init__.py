import datetime as dt
import websockets
import asyncio
import jwt
import json
import threading
from ThreadPoolExecutorPlus import ThreadPoolExecutor

from app.db_config import database_connect_mongo
from app.ChatBotModels import chatbotObj
from app.db_chatHist import db_hist



objChat = chatbotObj()
objdb_hist = db_hist()




# A set of connected ws clients
connected = set()


# def create_app(config_name):

async def create_app(websocket, path):
    print("A client just connected")
    # Store a copy of the connected client
    
    # Handle incoming messages
    try:
        async for message in websocket:
            # string to json convert
            message = json.loads(message)

            # Date Time stamp
            dateTime = str(dt.datetime.now())
            dateTime = dateTime[:dateTime.rfind('.')]
            print('=============Chatbot=========')

            print(type(message))

            if type(message)==dict:
                # chatbot object call
                objChat.main(message)
                objResponse = objChat.response()
                objResponse['datetime'] = dateTime
                stopList = ['ChatId successful expired']

                # chat history save
                if "chatid" in list(message.keys()) and objResponse['message'] not in stopList:
                    data = {'chatId': message['chatid'], 'response': "chatbot", 'text':objResponse}
                    objdb_hist.chat_history(action="update", data=data, status="ACTIVE")

                # print input and output request
                print(dateTime,message)
                print('-----------------------------')
                print(dateTime,objResponse)
                # print('---------------------')
                # print(objChat.roomDetails)
                print('=============================')
                print(objChat.chatTemp)
                print('-----------------------------')
                print(len(objChat.roomDetails))
                

                # websocket closed
                try:
                    res_stop = objResponse['data']['option'] if objResponse['data'] not in [None,"None"] else None
                except Exception as e:
                    res_stop = None

                

                if res_stop=="stop":#objResponse["message"] == "ChatId successful expired" or 

                    await websocket.send(json.dumps(objResponse))

                    # send Mail
                    for sm in objdb_hist.tempData:
                        try:
                            # with ThreadPoolExecutor(max_workers = 10) as executor:
                            #     executor.set_daemon_opts(min_workers = 2 , max_workers = 10 , keep_alive_time = 10)
                            #     sendMail_thread = executor.submit(objdb_hist.email_notification, userId=sm['userId'],name=sm['name'],crno=sm['crno'],timestamp=sm['timestamp'])
                            #     print(sendMail_thread.result())

                            sendMail_thread = threading.Thread(target=objdb_hist.email_notification, name="sendMail", args=(sm['userId'],sm['name'],sm['crno'],sm['timestamp'],sm['chatId']))
                            sendMail_thread.start()
                            # sendMail_thread.join()
                            # sendMail = objdb_hist.email_notification(userId=sm['userId'],name=sm['name'],crno=sm['crno'],timestamp=sm['timestamp'])
                                
                        except Exception as e:
                            print('sendMail Error: ',str(e))

                    # await websocket.send(json.dumps(objResponse))
                    
                    await websocket.close(1011, "authentication failed")

                elif res_stop=="stop_no_email":
                    await websocket.send(json.dumps(objResponse))
                    await websocket.close(1011, "authentication failed")


                await websocket.send(json.dumps(objResponse))
                # await websocket.send("its working")
                
            else:
                # await websocket.send("websocket close")
                await websocket.close(1011, "authentication failed")
                
                
            
            # Send a response to all connected clients except sender
            
            # await websocket.send(f"Someone said: {message}")
    # Handle disconnecting clients 
    # except Exception as e:
    except websockets.exceptions.ConnectionClosed as e:
        print("A client just disconnected")
        await websocket.close(1011, "authentication failed")
        #print("Error: ",str(e))
    finally:
        pass
        # connected.remove(websocket)

    # return app
