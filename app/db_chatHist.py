from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import smtplib
import os
import threading
import time

from app.db_config import database_connect_mongo
from app.db_config import *
from bson import ObjectId
import datetime as dt


class db_hist:

    tempData = []

    def __init__(self):
        self.db = database_connect_mongo()

    def database(self, req={}):
        response = {'status_code': 200, "message": "successful", "data": []}
        # ['insert','select','find','update']
        # {"order":"select","data":{"table":"tableName","condition":"find"}}

        try:
            # select data in database
            if req['order'] == 'select':
                db = self.db[req['data']['table']]
                tbl_result_1 = list()
                for each in db.find():
                    each['_id'] = str(each['_id'])
                    tbl_result_1.append(each)

                response['message'] = "successful select all"
                response['data'] = tbl_result_1

            # find data in database
            elif req['order'] == 'find':
                db = self.db[req['data']['table']]
                tbl_result_2 = db.find_one(req['data']['condition'])
                tbl_result_2['_id'] = str(tbl_result_2['_id'])

                response['message'] = "successful find"
                response['data'] = tbl_result_2

            # find all data in database
            elif req['order'] == 'findAll':
                db = self.db[req['data']['table']]
                tbl_result_2 = db.find(req['data']['condition'])
                tbl_result_1 = list()
                for each in tbl_result_2:
                    each['_id'] = str(each['_id'])
                    tbl_result_1.append(each)

                response['message'] = "successful find"
                response['data'] = tbl_result_1

            # insert data in database
            elif req['order'] == 'insert':
                db = self.db[req['data']['table']]
                tbl_result_3 = db.insert_one(req['data']['condition'])

                response['message'] = "successful insert"
                response['data'] = tbl_result_3

            # update data in database
            elif req['order'] == 'update':
                db = self.db[req['data']['table']]
                newJson = {"$set": req['data']['condition']['new']}
                oldJson = req['data']['condition']['old']
                tbl_result_4 = db.update_one(oldJson, newJson, upsert=False)

                response['message'] = "successful update"
                response['data'] = tbl_result_4

            else:
                response['message'] = "No Data"
                response['data'] = []
                response['status_code'] = 400
        except Exception as e:
            response['message'] = "No Data"
            response['data'] = []
            response['status_code'] = 400

        return response

    def chat_history(self, action=None, data={}, status="ACTIVE"):
        response = {'status_code': 200, "message": "successful", "data": []}

        # Time Stamp
        timestamp = str(dt.datetime.now())
        timestamp = timestamp[:timestamp.rfind('.')]

        # action=insert/update
        # if update data={"chatId":roomId, response:chatbot/user, text:id/text}
        # if insert data={'chatId':roomId,'name':getName,'cr_no':getCrNo,'userId':getId}
        # if update data={'chatId':roomId,response:status}, status=inactive

        if action == 'insert' and len(data) == 4:
            chatHist = {
                "chat_id": data['chatId'],
                "user_id": data['userId'],
                "cr_no": data['cr_no'],
                "name": data['name'],
                "data": [],
                "status": status,
                "time_stamp":timestamp
            }
            insertUserHistData = {"order": "insert", "data": {"table": "tbl_UserChatHistory", "condition": chatHist}}
            insertUserHist = self.database(insertUserHistData)

        elif action == 'update':

            timestamp = str(dt.datetime.now())
            timestamp = timestamp[:timestamp.rfind('.')]

            selectUserHistData = {"order": "find",
                                  "data": {"table": "tbl_UserChatHistory", "condition": {"chat_id": data['chatId']}}}
            selectUserHist = self.database(selectUserHistData)

            updateUserHistId = selectUserHist['data']['_id']
            updateUserHistData = selectUserHist['data']['data']
            # updateUserHistStatus = selectUserHist['data']['status']

            # Update ChatHistory
            if data['response'] == 'chatbot':
                updateUserHistData.append({'response_id': data['text'], "timestamp": timestamp})
                PrepareData = {"old": {"_id": ObjectId(updateUserHistId)}, "new": {"data": updateUserHistData}}
                updateUserHistData = {"order": "update",
                                      "data": {"table": "tbl_UserChatHistory", "condition": PrepareData}}
                self.database(updateUserHistData)

            # Update ChatHistory
            if data['response'] == 'user':
                updateUserHistData.append({'request_text': data['text'], "timestamp": timestamp})
                PrepareData = {"old": {"_id": ObjectId(updateUserHistId)}, "new": {"data": updateUserHistData}}
                updateUserHistData = {"order": "update",
                                      "data": {"table": "tbl_UserChatHistory", "condition": PrepareData}}
                self.database(updateUserHistData)

            if data['response'] == 'status':
                PrepareData = {"old": {"_id": ObjectId(updateUserHistId)}, "new": {"status": status}}
                updateUserHistData = {"order": "update",
                                      "data": {"table": "tbl_UserChatHistory", "condition": PrepareData}}
                self.database(updateUserHistData)

        return response


    def email_notification(self,userId=None,name=None,crno=None,timestamp=None,chatId=None):
        url = "http://chatbot.rgci-letstalk.com"

        # print("=========EmailSend=========")
        # print(chatId)
        

        # localVariable data remove
        for i,sm in enumerate(self.tempData):
            if name==sm['name'] and crno==sm['crno']:
                del self.tempData[i]

        
        # Bind link for show user history
        bindLinkData = {"order": "find", "data": {"table": "tbl_UserChatHistory", "condition": {"chat_id":chatId}}}
        bindLink = self.database(bindLinkData)
        
        if bindLink['status_code'] == 200:
            siteLink = f"{WEB_URL}/doctor/conversation/{bindLink['data']['user_id']}/{bindLink['data']['_id']}"
            # siteLink = f"http://updateapplications.com/letstalk/admin/#/members/conversation/{userId}/{bindLink['data']['_id']}"
        else:
            siteLink = f"{WEB_URL}/doctor"
            # siteLink = "https://updateapplications.com/letstalk/admin/#/login"
            

        # Patient and Doctor Details
        PatientDoctorData = {"order": "findAll", "data": {"table": "doctor_patients", "condition": {"patient_id":userId,"status":"ACTIVE"}}}
        PatientDoctor = self.database(PatientDoctorData)
        DataPD = []
        DataDoc = []


        for eId in PatientDoctor['data']:
            findDoctorData = {"order": "find", "data": {"table": "user", "condition": {"_id":ObjectId(eId["doctor_id"]),"status":"ACTIVE"}}}
            findDoctor = self.database(findDoctorData)
            if findDoctor['data']:
                DataPD.append(findDoctor['data']['email_id'])
                
                

        findUserDocData = {"order": "findAll", "data": {"table": "user_documents", "condition": {"user_id":userId,"status":"ACTIVE"}}}
        findUserDoc = self.database(findUserDocData)
        if findUserDoc['data']!=None:
            for doc in findUserDoc['data']:
                for d in doc['files']:
                    DataDoc.append(f"<a href='{API_URL}/{d}' download>{d[d.rfind('/')+1:]}</a>")


        # Select all doctor mail id related patient
        DataPD = [x for x in DataPD if x not in [None,"None",""]]
        # email = ", ".join(DataPD)
        # email = "sankhya@yopmail.com"
        # print(email)
        DataDoc = " <br>".join(DataDoc)
        # print(DataDoc)
        
        # name,email,otp="shakya", "shakya.ivaninfotech@gmail.com", "102536"
        
        sender_email = SENDER_EMAIL
        receiver_email = DataPD
        password = SENDER_PASS

        # Create MIMEMultipart object
        msg = MIMEMultipart("alternative")
        msg["Subject"] = f"{name} completed chat on date time: {timestamp}"
        msg["From"] = sender_email
        # msg["To"] = receiver_email
        html = f"""
        <div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
          <div style="margin:50px auto;width:70%;padding:20px 0">
            <div style="border-bottom:1px solid #eee">
              <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">Completed Chat</a>
            </div>
            <p style="font-size:1.1em">Hello,</p>
            <p>Patient ({crno}) completed a chat on {timestamp}. Please check chat details by follwing link.</p>
            <p>Link :<a href="{siteLink}" download>{siteLink}</a></p>
            <p>Documents :{DataDoc}</p>
            <hr style="border:none;border-top:1px solid #eee" />
            <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300">
            </div>
          </div>
        </div>"""
        part = MIMEText(html, "html")
        msg.attach(part)

        # # open the file to be sent 
        # static/user_documents/630e24c8240da0832440d827/41828-BKQC.pdf
        # filename = "demo.txt"
        # attachment = open("app/demo.txt", "rb")

        # # instance of MIMEBase and named as p
        # p = MIMEBase('application', 'octet-stream')

        # # To change the payload into encoded form
        # p.set_payload((attachment).read())

        # # encode into base64
        # encoders.encode_base64(p)
           
        # p.add_header('Content-Disposition', "attachment; filename= %s" % filename)
          
        # # attach the instance 'p' to instance 'msg'
        # msg.attach(p)


        try:
            smtpObj = smtplib.SMTP_SSL(SENDER_HOST)
            # print(dir(smtpObj))
            smtpObj.login(sender_email, password, initial_response_ok=True)
            # time.sleep(1)

            # smtpObj.sendmail(sender_email, " ,".join(receiver_email), msg.as_string())

            # send mail one by one
            for receiver_emailId in receiver_email:
                msg["To"] = receiver_emailId
                print(receiver_emailId)
                smtpObj.sendmail(sender_email, receiver_emailId, msg.as_string())

            if smtpObj:
                smtpObj.close()

            print("message sent through email")
            response = {"status": 'success', "message": 'message sent through email'}
            return response
        except Exception as e:
            import traceback
            traceback.print_exc()
            response = {"status": 'error', "message": f'{str(e)}'}
            return response

    def valueStore(self,userId=None,name=None,crno=None,timestamp=None,chatId=None):

        if userId not in [x['userId'] for x in self.tempData if x]:
            self.tempData.append({"userId":userId,"name":name,"crno":crno,"timestamp":timestamp,"chatId":chatId})

        else:
            for i,Ids in enumerate(self.tempData):
                if Ids['userId'] == userId:
                    self.tempData[i]['chatId'] = chatId
                    self.tempData[i]['name'] = name
                    self.tempData[i]['crno'] = crno
                    self.tempData[i]['timestamp'] = timestamp



        return None
    
    def archive(self,ids=None):
        response = {'status':True,'data':None}

        try:
            archiveHistData = []
            archiveData = {"order": "find","data": {"table": "tbl_UserChatHistory", "condition": {"_id":ObjectId(ids)}}}
            archive = self.database(archiveData)            
            
            if archive['status_code']==200:
                for hist in archive['data']['data']:
                    archiveHistData.append(hist['response_id'])

                ChatId = archive['data']['chat_id']

                chatTemp = []

                for each in archiveHistData:
                    if each['status_code']==200:
                        if each['data']['action'] not in [None,'','null']:
                            if len(chatTemp)!=0:
                                for add in chatTemp:
                                    if add['roomId']!=ChatId:
                                        chatTemp.append({'roomId':ChatId})
                                        
                            if len(chatTemp)==0:
                                chatTemp.append({'roomId':ChatId})
                                        
                                    
                            actionType = each['data']['action']
                            actionTypeSplit = [x.lower() for x in actionType.split('|') if x]
                            
                            if 'hold' in actionTypeSplit:
                                for hold in chatTemp:
                                    if hold['roomId']==ChatId:
                                        hold['responseList'] = actionType
                                        hold['status'] = 'ACTIVE'
                                        break
                                        
                            elif 'holdDuration' in actionType:
                                for holdD in chatTemp:
                                    if holdD['roomId']==ChatId:
                                        holdD['duration'] = [x['name'] for x in each['data']['response_text'] if x['action_type']=='button']
                                        holdD['status'] = 'ACTIVE'
                                        break
                                        
                            elif 'holdpi1' in actionTypeSplit:
                                for hold in chatTemp:
                                    if hold['roomId']==ChatId:
                                        hold['responseList'] = actionType
                                        hold['status'] = 'ACTIVE'
                                        break
                                        
                            elif 'holdpi2' in actionTypeSplit:
                                for hold in chatTemp:
                                    if hold['roomId']==ChatId:
                                        hold['responseList'] = actionType
                                        hold['status'] = 'ACTIVE'
                                        break
                                        
                                        
                        
                        if len(chatTemp)!=0:
                            for setVal in chatTemp:
                                if setVal['roomId'] == ChatId and 'responseList' in list(setVal.keys()):
                                    if setVal['responseList'] not in ['null','',None]:
                                        
                                        if each['data']['option'] in [x.lower() for x in setVal['responseList'].split('|') if x]\
                                            and 'hold' in [x.lower() for x in setVal['responseList'].split('|') if x]:
                                            
                                            setVal['status'] = 'INACTIVE'
                                            setVal['text'] = each['data']['response_text'][0]['name']
                                            setVal['responseList'] = ''
                                            break
                                            
                                        
                                        if each['data']['option'] in [x.lower() for x in setVal['responseList'].split('|') if x]\
                                            and 'holdpi1' in [x.lower() for x in setVal['responseList'].split('|') if x]:
                                            
                                            setVal['status'] = 'INACTIVE'
                                            setVal['patient_d1'] = each['data']['response_text'][0]['name']
                                            setVal['responseList'] = ''
                                            break
                                            
                                        if each['data']['option'] in [x.lower() for x in setVal['responseList'].split('|') if x]\
                                            and 'holdpi2' in [x.lower() for x in setVal['responseList'].split('|') if x]:
                                            
                                            setVal['status'] = 'INACTIVE'
                                            setVal['patient_d2'] = each['data']['response_text'][0]['name']
                                            setVal['responseList'] = ''
                                            break
                                            
                                            
                                            
                                if setVal['roomId'] == ChatId and 'duration' in list(setVal.keys()):
                                    if each['data']['response_text'][0]['name'] in setVal['duration']:
                                        setVal['status'] = 'INACTIVE'
                                        setVal['duration'] = [each['data']['response_text'][0]['name']]
                                        break
                                        

                        if each['data']['option'] in ['stop'] or each['data']['response_id'] in [21]:
                            for i,rem in enumerate(chatTemp):
                                if rem['roomId']==ChatId:
                                    del chatTemp[i]
                                    break



                response['status'] = True
                response['data'] = {'roomId':ChatId,'chatHist':archiveHistData,'chatTemp':chatTemp}
                
        except Exception as e:
            print('Archive id error')
            response['status'] = False
            response['data'] = None
            print('archive_error_MOdule: ',str(e))
        

        return response

