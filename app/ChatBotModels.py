# db = database_connect_mongo()
# db = db["tbl_chatbot_2"]
import jwt
from bson.objectid import ObjectId
import datetime as dt
import nltk
from nltk.chat.util import Chat, reflections
from app.db_config import database_connect_mongo
from app.db_config import *
from app.userDetailsReq import userDetails
from app.PatientCheck import PatientCondition
from app.db_chatHist import db_hist
from app.userTextCheck import text



class chatbotObj:
    roomDetails = []
    chatResponseData = []
    chatTemp = []
    
    
    def main(self, token=None):
        self.token = token
        self.db = database_connect_mongo()
        self.secretKey = "secret"
        self.expireTime = 1800 #1800=30min
        # url = "http://chatbot.rgci-letstalk.com"
        # url = "http://letstalk.dev13.ivantechnology.in"
        self.obj = userDetails(urls=API_URL)
        self.objPC = PatientCondition()
        self.objdb_hist = db_hist()
        self.obj_textCheck = text()

        
    def response(self):
        response = {'status_code':200, "message":"successful","data":[]}
        #====================================================================
        # chat data set into localVariable one time
        if len(self.chatResponseData) == 0:
            dataSet = {"order":"select","data":{"table":"tbl_chatbot_1","condition":{}}}
            getChatData = self.database(dataSet)
            for tbl_1 in getChatData['data']:
                self.chatResponseData.append([tbl_1['request_text'],[tbl_1['response_id']]])
        
        # function call
        reqCheck = self.requesCheck()
        
        
        #self.model_nltk()
        if reqCheck['status_code']==200:
            if 'user' in reqCheck['data'].keys():
                nltkResult = self.model_nltk(reqCheck['data']['user'])
                if nltkResult['data']==None or nltkResult['status_code'] == 400:
                    response['data'] = None
                    response['status_code'] = 200
                    response['message'] = nltkResult['message']
                    # response['message_type'] = """not_understand"""
                    
                    if  len([x for x in self.chatTemp if x['roomId'] == self.token['chatid'] and x['status'] != "INACTIVE" ])!=0:
                        chatHold = self.chat_hold()
                        response['data'] = chatHold['data']
                        response['status_code'] = chatHold['status_code']
                        response['message'] = chatHold['message']
                        
                    else:
                        response['data'] = nltkResult['data']
                        response['status_code'] = 200
                        response['message'] = 'successful'
                        # response['message_type'] = """not_understand"""
                


                # elif nltkResult['status_code'] == 201:
                #     response['data'] = nltkResult['data']
                #     response['message'] = nltkResult['message']
                #     response['status_code'] = 200
                #     response['message_type'] = """not_understand"""
                         
                else:

                    reqCondition = self.condition(nltkResult['data'])
                    response['data'] = reqCondition
            else:

                response['data'] = reqCheck['data']
                response['message'] = reqCheck['message']
                
        elif reqCheck['status_code']==201:
            response['data'] = reqCheck['data']
            response['status_code'] = 200
            response['message'] = reqCheck['message']

            
        else:

            response['data'] = reqCheck['data']
            response['status_code'] = 400
            response['message'] = reqCheck['message']
            
        
        return response
    
    def database(self, req={}):
        response = {'status_code':200, "message":"successful","data":[]}
        #['insert','select','find','update']
        #{"order":"select","data":{"table":"tableName","condition":"find"}}
        
        # try:
        # select data in database
        if req['order'] == 'select':
            db = self.db[req['data']['table']]
            tbl_result_1 = list()
            for each in db.find():
                each['_id'] = str(each['_id'])
                tbl_result_1.append(each)
                
            response['message'] = "successful select all"
            response['data'] = tbl_result_1
                
        # find data in database
        elif req['order'] == 'find':
            db = self.db[req['data']['table']]
            tbl_result_2 = db.find_one(req['data']['condition'])
            tbl_result_2['_id'] = str(tbl_result_2['_id'])
                
            response['message'] = "successful find"
            response['data'] = tbl_result_2
            
        # insert data in database
        elif req['order'] == 'insert':
            db = self.db[req['data']['table']]
            tbl_result_3 = db.insert_one(req['data']['condition'])
                
            response['message'] = "successful insert"
            response['data'] = tbl_result_3
        
        # update data in database
        elif req['order'] == 'update':
            db = self.db[req['data']['table']]
            newJson = {"$set": req['data']['condition']['new']}
            oldJson = req['data']['condition']['old']
            tbl_result_4 = db.update_one(oldJson,newJson, upsert=False)
            
            response['message'] = "successful update"
            response['data'] = tbl_result_4
            
        else:
            response['message'] = "No Data"
            response['data'] = []
            response['status_code'] = 400
        # except Exception as e:
        #     response['message'] = "No Data"
        #     response['data'] = []
        #     response['status_code'] = 400
                
        return response
    
    def condition(self, responseData=None):
        response = {'status_code':200, "message":"successful","data":[]}
        
        #Set temp values self.chatTemp
        
        if responseData['action'] != None and 'hold' in [x.lower() for x in responseData['action'].split('|')]:
            data = {"roomId":self.token['chatid'],"responseList":responseData['action'],"status":"ACTIVE"}
            self.chatTemp.append(data)
            
        
        if responseData['action'] != None and 'holdduration' in [x.lower() for x in responseData['action'].split('|') if x]:
            for setDate in self.chatTemp:
                if setDate['roomId']==self.token['chatid']:
                    setDate['duration'] = [x['name'] for x in responseData['response_text'] if x['action_type']=='button']
                    setDate['status'] = "ACTIVE"

        if responseData['action'] != None and 'holdpi1' in [x.lower() for x in responseData['action'].split('|') if x]:
            for setDate in self.chatTemp:
                if setDate['roomId']==self.token['chatid']:
                    setDate['responseList'] = responseData['action']
                    setDate['status'] = "ACTIVE"

        if responseData['action'] != None and 'holdpi2' in [x.lower() for x in responseData['action'].split('|') if x]:
            for setDate in self.chatTemp:
                if setDate['roomId']==self.token['chatid']:
                    setDate['responseList'] = responseData['action']
                    setDate['status'] = "ACTIVE"

        if responseData['action'] != None and 'forcestop' in [x.lower() for x in responseData['action'].split('|') if x]:
            responseData['action'] = None
            responseData['option'] = "check_condition"
            # responseData['message_type'] = "stop_tki"
            # responseData['m_type'] = "stop_tki"

            for i,setDate in enumerate(self.chatTemp):
                if setDate['roomId']==self.token['chatid']:
                    del self.chatTemp[i]



        if responseData['action'] != None and 'stop' in [x.lower() for x in responseData['action'].split('|') if x]:

            for i,setDate in enumerate(self.chatTemp):
                if setDate['roomId']==self.token['chatid']:
                    setDate['responseList'] = responseData['action']
                    objSetV = self.objPC.check(setDate)
                    responseData['action'] = None
                    responseData['option'] = objSetV['res3'] #stop|continue
                    responseData['response_text'][0]['name'] = objSetV['res2']
                    if objSetV['res3']=="stop":
                        responseData['message_type'] = "stop_tki"
                        responseData['m_type'] = "stop_tki"
                        responseData['option'] = "continue"
                        
                        # send mail variable set 
                        # for sm,user in enumerate(self.roomDetails):
                        #     if user['roomId'] == self.token['chatid']:
                        #         mailSend = self.objdb_hist.valueStore(userId=self.roomDetails[sm]['userId'],
                        #         name=self.roomDetails[sm]['name'],crno=self.roomDetails[sm]['crNo'],
                        #         timestamp=self.roomDetails[sm]['timeStamp'],
                        #         chatId=self.token['chatid'])

                    # data = {'chatId': self.token['chatid'], 'response': "status"}
                    # self.objdb_hist.chat_history(action="update", data=data, status="INACTIVE")
                    
                    del self.chatTemp[i]


        
        #get User Details from chatId
        userChatId = self.token['chatid']
        getRoomDetails = [x for x in self.roomDetails if x['roomId']==userChatId]
        
        #get User Details from DB
        dataUserFind = {"order":"find","data":{"table":"user","condition":{'_id':ObjectId(getRoomDetails[0]['userId'])}}}
        getUserRes = self.database(dataUserFind)
        
        #Get User Details and set values in variable
        name = getUserRes['data']['name']
        email = getUserRes['data']['email_id']
        phone = getUserRes['data']['phone_number']
        dob = getUserRes['data']['dob']
        sex = getUserRes['data']['sex']
        
        # Values changes
        try:
            
            for i,check in enumerate(responseData['response_text']):

                # userDetailsGet
                GetUserDetails = [x for x in self.roomDetails if x['roomId']==self.token['chatid']]
                GetUserDetails = GetUserDetails if len(GetUserDetails)==0 else GetUserDetails[0]
                
                userId = GetUserDetails['userId']
                token = GetUserDetails['token']

                # values split like | example name|phone|dob = [name,phone,dob]
                setVal = [x for x in str(check['setValue']).split('|') if x]
                

                for setV in setVal:
                    #===========Name Set===========
                    if setV.lower() == 'name':
                        check['name'] = check['name'] %(name)

                    #===========Vital Set===========
                    if setV.lower() == 'vitals':
                        
                        vitalsList = self.obj.vitals(token=token, data={"user_id":userId})
                        check['name'] = vitalsList

                    #===========Exist Site Effect Set===========
                    try:
                        if setV.lower() == 'existsideeffect':
                            existsideeffect = self.obj.sideEffects(token=token, data={"user_side_id":userId}, req='ACTIVE')
                            reqTextCheck = self.obj.showAllreq(text=existsideeffect)

                            if len(existsideeffect) != 0:
                                for esf in existsideeffect:

                                    # responseData['response_text'].append(esf)

                                    if esf['name'] == "Absence of data":
                                        responseData['option'] = "next&28&"
                                        responseData['action'] = None
                                        responseData['message_type'] = "no_side_effects"
                                        esf['name'] = "There are no exist side effects please select from below"
                                        esf['action_type'] = "no_side_effect_text"
                                        responseData['response_text'].append(esf)

                                        for i, remText in enumerate(responseData['response_text']):
                                            if remText['action_type'] in ['text']:
                                                del responseData['response_text'][i]

                                        for i,setData in enumerate(self.chatTemp):
                                            if setData['roomId']==self.token['chatid']:
                                                del self.chatTemp[i]

                                    elif esf['name'] != "Absence of data":
                                        responseData['option'] = None
                                        responseData['action'] = "hold|next&12&"
                                        responseData['response_text'].append(esf)


                                    # else:

                                    #     if esf['name'] != "Absence of data" and reqTextCheck==True:
                                    #         responseData['option'] = None
                                    #         responseData['action'] = "hold|next&12&"
                                    #         responseData['response_text'].append(esf)

                                    #     if esf['name'] != "Absence of data" and reqTextCheck==False:
                                    #         responseData['option'] = "next&28&"
                                    #         responseData['action'] = None
                                    #         responseData['message_type'] = "no_side_effects"
                                    #         esf['name'] = "There are no exist side effects please select from below"
                                    #         esf['action_type'] = "no_side_effect_text"

                                    #         responseData['response_text'].append(esf)

                                    #         for i,setData in enumerate(self.chatTemp):
                                    #             if setData['roomId']==self.token['chatid']:
                                    #                 del self.chatTemp[i]

                    except Exception as e:
                        print('error4',str(e))
                        

                    #===========Exist Site Effect Set===========
                    if setV.lower() == 'previoussideeffect':
                        
                        try:
                            previoussideeffect = self.obj.sideEffects(token=token, data={"user_side_id":userId}, req='INACTIVE')
                            reqTextCheck = self.obj.showAllreq(text=previoussideeffect)

                            if len(previoussideeffect) != 0:
                            
                                for esf in previoussideeffect:

                                    if esf['name'] == "Absence of data":
                                        responseData['option'] = "next&29&"
                                        responseData['action'] = None
                                        responseData['message_type'] = "no_side_effects"
                                        esf['name'] = "There are no previous side effects please select from below"
                                        esf['action_type'] = "no_side_effect_text"
                                        responseData['response_text'].append(esf)

                                        for i, remText in enumerate(responseData['response_text']):
                                            if remText['action_type'] in ['text']:
                                                del responseData['response_text'][i]

                                        for i,setData in enumerate(self.chatTemp):
                                            if setData['roomId']==self.token['chatid']:
                                                del self.chatTemp[i]

                                    elif esf['name'] != "Absence of data":
                                        responseData['option'] = None
                                        responseData['action'] = "hold|next&12&"
                                        responseData['response_text'].append(esf)


                                    # elif esf['name'] != "Absence of data" and reqTextCheck==False:
                                    #     responseData['option'] = "next&29&"
                                    #     responseData['action'] = None
                                    #     responseData['message_type'] = "no_side_effects"
                                    #     esf['name'] = "There are no previous side effects please select from below"
                                    #     esf['action_type'] = "no_side_effect_text"
                                    #     responseData['response_text'].append(esf)

                                    #     for i,setData in enumerate(self.chatTemp):
                                    #         if setData['roomId']==self.token['chatid']:
                                    #             del self.chatTemp[i]
                                                
                                    # else:


                                    #     if esf['name'] != "Absence of data" and reqTextCheck==True:
                                    #         responseData['option'] = None
                                    #         responseData['action'] = "hold|next&12&"
                                    #         responseData['response_text'].append(esf)

                                    #     if esf['name'] != "Absence of data" and reqTextCheck==False:
                                    #         responseData['option'] = "next&29&"
                                    #         responseData['action'] = None
                                    #         responseData['message_type'] = "no_side_effects"
                                    #         esf['name'] = "There are no previous side effects please select from below"
                                    #         esf['action_type'] = "no_side_effect_text"

                                    #         responseData['response_text'].append(esf)

                                    #         for i,setData in enumerate(self.chatTemp):
                                    #             if setData['roomId']==self.token['chatid']:
                                    #                 del self.chatTemp[i]

                        except Exception as e:
                            print('error5',str(e))
                                           
        except Exception as e:
            print('error1: ',str(e))
            responseData = responseData
        
        return responseData #getChatRes['data']

    
    def requesCheck(self):
        response = {'status_code':200, "message":"successful","data":[]}
        
        if 'token' in self.token.keys():
            try:
                # Decoded Token
                decoded_token = jwt.decode(str(self.token['token']), self.secretKey, algorithms=['HS256'])
                
                # userId find in userTable and get '_id'
                orderFind = {"order":"find","data":{"table":"user","condition":{"_id":ObjectId(decoded_token['_id'])}}}
                getValue = self.database(orderFind)['data']
                getId = str(getValue['_id'])
                getName = getValue['name']
                getCrNo = getValue['cr_no']
                
                # create room id for chat
                dateTime = str(dt.datetime.now())
                dateTime = dateTime[:dateTime.rfind('.')]
                dateTime = '%s&'+dateTime.replace(' ','&').replace('-','&').replace(':','&')
                roomId = dateTime %(getCrNo.replace(' ','&'))
                
                timestamp = str(dt.datetime.now())
                timestamp = timestamp[:timestamp.rfind('.')]

                # remove same user in local variable
                # if len(self.roomDetails)>=10:
                #     for i,userDetail in enumerate(self.roomDetails):
                #         if self.roomDetails[i]["userId"] == getId:
                #             del self.roomDetails[i]
                # roomId = "242375&2022&07&22&15&03&04"

                # Archive Chat
                archiveHistData = []
                if 'archive' in self.token.keys() and self.token['archive'] not in [None,"","None"]:
                    # roomId = "242375&2022&07&22&15&03&04"
                    ids = self.token['archive']

                    # modul call
                    try:
                        achiveModule = self.objdb_hist.archive(ids)
                        if achiveModule['status']==True:
                            roomId = achiveModule['data']['roomId']
                            archiveHistData = achiveModule['data']['chatHist']
                            # print('=======##############=======')
                            if achiveModule['data']['chatTemp']:
                                for ct in self.chatTemp:
                                    if ct['roomId']!=achiveModule['data']['chatTemp'][0]['roomId']:
                                        self.chatTemp.append(achiveModule['data']['chatTemp'][0])
                                else:
                                    self.chatTemp.append(achiveModule['data']['chatTemp'][0])
                            # print(achiveModule['data']['chatTemp'])
                            # print('############################')
                    except Exception as e:
                        print('Archive id error')
                        

                for Ids in self.roomDetails:
                    if Ids['userId']==getId:
                        Ids['name'] = getName
                        Ids['roomId'] = roomId
                        Ids['token'] = str(self.token['token'])
                        Ids['timeStamp'] = timestamp
                        # print('=======User Update======')
                    if Ids['userId']!=getId:
                        # print('=======User Add======')
                        # insert into localVariable
                        self.roomDetails.append({'userId':getId,"name":getName,"token":str(self.token['token']),"roomId":roomId,'crNo':getCrNo,'timeStamp':timestamp})
                
                else:
                    if len(self.roomDetails)==0:
                        # print('=======User New======')
                        # insert into localVariable
                        self.roomDetails.append({'userId':getId,"name":getName,"token":str(self.token['token']),"roomId":roomId,'crNo':getCrNo,'timeStamp':timestamp})
                        
                
                
                #self.roomDetails.append({'userId':getId,"name":getName,"roomId":'242375&2022&07&22&15&03&04'})
                
                # chat history object create
                if 'archive' in self.token.keys() and self.token['archive'] not in [None,"","None"]:
                    try:
                        data = {'chatId': roomId, 'response': "status"}
                        self.objdb_hist.chat_history(action="update", data=data, status="ACTIVE")
                    except:
                        chatHistStart = {'chatId':roomId,'name':getName,'cr_no':getCrNo,'userId':getId}
                        self.objdb_hist.chat_history(action='insert',data=chatHistStart)
                else:
                    chatHistStart = {'chatId':roomId,'name':getName,'cr_no':getCrNo,'userId':getId}
                    self.objdb_hist.chat_history(action='insert',data=chatHistStart)
                

                # ChatTemp id remove from localVariable
                # if self.chatTemp:
                #     for i,ct in enumerate(self.chatTemp):
                #         if ct['status']=='INACTIVE':
                #             del self.chatTemp[i]

                #chatHistdata = {'chatId':'242375&2022&07&22&15&03&04','response':'chatbot','text':'Testing_1'}
                #chatHistdata = {'chatId':'242375&2022&07&22&15&03&04','response':'status'}
                
                #self.chat_history(action='insert',data=chatHistdata)
                #self.chat_history(action='update',data=chatHistdata,status='active')
                
                response['status_code'] = 200
                response['message'] = "Token is Valid"
                response['data'] = {"token":"valid","status":True,"archive":archiveHistData,"chatid":roomId}
            except Exception as e:
                print('error2',str(e))
                response['status_code'] = 400
                response['message'] = "Token is Not Valid"
                response['error_message'] = str(e)
                response['data'] = {"token":"not valid","status":False}
                
        
        elif 'user' in self.token.keys():

            nowTime = str(dt.datetime.now())
            nowTime = nowTime[:nowTime.rfind('.')]

            # ChatId expired using time
            for i,user in enumerate(self.roomDetails):
                joinTime = dt.datetime.fromisoformat(str(user['timeStamp']))
                nowTime = dt.datetime.fromisoformat(str(nowTime))
                time_check = nowTime - joinTime

                if int(time_check.total_seconds()) >= self.expireTime:
                    # print('token expired')
                    if self.chatTemp:
                        for i,ct in enumerate(self.chatTemp):
                            if ct['roomId']==self.roomDetails[i]['roomId']:
                                del self.chatTemp[i]

                    del self.roomDetails[i]
                else:

                    # print('token working')
                    user['timeStamp'] = nowTime
                    
                    
            # ChatId Expired
            if self.token['chatid']==self.token['user']:
                for i,user in enumerate(self.roomDetails):
                    if user['roomId'] == self.token['chatid']:

                        # send mail variable set
                        # mailSend = self.objdb_hist.valueStore(userId=self.roomDetails[i]['userId'],
                        # name=self.roomDetails[i]['name'],crno=self.roomDetails[i]['crNo'],
                        # timestamp=self.roomDetails[i]['timeStamp'],
                        # chatId=self.token['chatid'])

                        data = {'chatId': self.token['chatid'], 'response': "status"}
                        self.objdb_hist.chat_history(action="update", data=data, status="IDLE")

                        # remove room details
                        del self.roomDetails[i]
                        break

                # remove room chatTemp
                if self.chatTemp:
                    for i,ct in enumerate(self.chatTemp):
                        if ct['roomId']==self.token['chatid']:
                            del self.chatTemp[i]

                chatIdExp = {'action': None,
                              'm_type': 'patient_request',
                              'message_type': 'patient_request',
                              'option': None,
                              'response_id': None,
                              'response_text': [{'action_type': 'title',
                                                 'name': 'Succesfully idle stop',
                                                 'setValue': None
                                                }]}


                response['status_code'] = 201
                response['message'] = "ChatId successful expired"
                response['data'] = chatIdExp

            elif str(self.token['user']).lower()=="stop_tki_email":
                for i,user in enumerate(self.roomDetails):
                    if user['roomId'] == self.token['chatid']:

                        # send mail variable set
                        mailSend = self.objdb_hist.valueStore(userId=self.roomDetails[i]['userId'],
                        name=self.roomDetails[i]['name'],crno=self.roomDetails[i]['crNo'],
                        timestamp=self.roomDetails[i]['timeStamp'],
                        chatId=self.token['chatid'])

                        data = {'chatId': self.token['chatid'], 'response': "status"}
                        self.objdb_hist.chat_history(action="update", data=data, status="INACTIVE")

                        # remove room details
                        del self.roomDetails[i]
                        break

                # remove room chatTemp
                if self.chatTemp:
                    for i,ct in enumerate(self.chatTemp):
                        if ct['roomId']==self.token['chatid']:
                            del self.chatTemp[i]

                chatIdExp = {'action': None,
                              'm_type': 'patient_request',
                              'message_type': 'chat_ended',
                              'option': 'stop',
                              'response_id': None,
                              'response_text': [{'action_type': 'title',
                                                 'name': ' Succesfully complete chat',
                                                 'setValue': None
                                                }]}


                response['status_code'] = 201
                response['message'] = "ChatId successful expired"
                response['data'] = chatIdExp

            elif str(self.token['user']).lower()=="stop_tki_no_email":
                for i,user in enumerate(self.roomDetails):
                    if user['roomId'] == self.token['chatid']:

                        # send mail variable set
                        # mailSend = self.objdb_hist.valueStore(userId=self.roomDetails[i]['userId'],
                        # name=self.roomDetails[i]['name'],crno=self.roomDetails[i]['crNo'],
                        # timestamp=self.roomDetails[i]['timeStamp'],
                        # chatId=self.token['chatid'])

                        data = {'chatId': self.token['chatid'], 'response': "status"}
                        self.objdb_hist.chat_history(action="update", data=data, status="INACTIVE")

                        # remove room details
                        del self.roomDetails[i]
                        break

                # remove room chatTemp
                if self.chatTemp:
                    for i,ct in enumerate(self.chatTemp):
                        if ct['roomId']==self.token['chatid']:
                            del self.chatTemp[i]

                chatIdExp = {'action': None,
                              'm_type': 'patient_request',
                              'message_type': 'chat_ended',
                              'option': 'stop_no_email',
                              'response_id': None,
                              'response_text': [{'action_type': 'title',
                                                 'name': ' Succesfully complete chat',
                                                 'setValue': None
                                                }]}


                response['status_code'] = 201
                response['message'] = "ChatId successful expired"
                response['data'] = chatIdExp

            elif self.token['chatid'] in [x['roomId'] for x in self.roomDetails if x]:
                # chatHistdata = {'chatId':self.token['chatid'],'response':'user','text':self.token['user']}
                # self.chat_history(action='update',data=chatHistdata)

                # Text Check
                text_check = self.obj_textCheck.checkTxt(self.token['user'])
                if text_check['status'] == True:
                    self.token['user'] = text_check['text']

                response['status_code'] = 200
                response['message'] = "User is Valid"
                response['data'] = self.token
                
            else:
                data = {'chatId': self.token['chatid'], 'response': "status"}
                self.objdb_hist.chat_history(action="update", data=data, status="INACTIVE")

                response['status_code'] = 400
                response['message'] = "ChatId expired"
                response['data'] = None
            
        else:
            print('Error else ',self.token)
            response['status_code'] = 400
            response['message'] = "Token expired"
            response['data'] = {"user":"not valid", "status":False}
        return response
    
    def model_nltk(self,text=None):
        response = {'status_code':200, "message":"successful","data":[]}
        chat = Chat(self.chatResponseData)
        
        try:
            if text != None and chat.respond(text):
                res = chat.respond(text)
                
                # Get Details related chatId
                dataFind = {"order":"find","data":{"table":"tbl_chatbot_2","condition":{'response_id':int(res)}}}
                getChatRes = self.database(dataFind)
                #getChatRes['data']['_id'] = str(getChatRes['data']['_id'])
                
                # update chatHistory
                # chatHistdata = {'chatId':self.token['chatid'],'response':'chatbot','text':getChatRes['data']}
                # self.chat_history(action='update',data=chatHistdata)
                
                response['status_code'] = 200
                response['data'] = getChatRes['data']

            else:
                # update chatHistory
                # chatHistdata = {'chatId':self.token['chatid'],'response':'chatbot','text':"""working..."""}
                # self.chat_history(action='update',data=chatHistdata)

                if len(self.roomDetails)!=0 and len([x for x in self.roomDetails if x['roomId']==self.token['chatid']]) != 0:
                    getHoldTextTemp = [x for x in self.chatTemp if x['roomId']==self.token['chatid']]
                    getHoldTextTemp = getHoldTextTemp if len(getHoldTextTemp)==0 else getHoldTextTemp[0]
                    
                    if len(getHoldTextTemp) == 5 and getHoldTextTemp['status']=='INACTIVE':
                        dataFind = {"order":"find","data":{"table":"tbl_chatbot_2","condition":{'response_id':int(21)}}}
                        getChatRes = self.database(dataFind)
                        getChatRes = getChatRes['data']
                        getChatRes['option'] = 'continue'
                        getChatRes['action'] = None
                        getChatRes['message_type'] = "stop_tki"
                        getChatRes['m_type'] = "stop_tki"

                        for i,setDate in enumerate(self.chatTemp):
                            if setDate['roomId']==self.token['chatid']:
                                del self.chatTemp[i]
                        
                        # data = {'chatId': self.token['chatid'], 'response': "status"}
                        # self.objdb_hist.chat_history(action="update", data=data, status="INACTIVE")
                        
                        # chatHistdata = {'chatId':self.token['chatid'],'response':'chatbot','text':getChatRes}
                        # self.chat_history(action='update',data=chatHistdata)

                        response['status_code'] = 200
                        response['data'] = getChatRes
                        
                    else:
                        unknown_text = {'action': None,
                              'm_type': 'not_understand',
                              'message_type': 'not_understand',
                              'option': "not_understand",
                              'response_id': None,
                              'response_text': [{'action_type': 'title',
                                                 'name': """I'm sorry, I can't understand you""",
                                                 'setValue': None
                                                }]}

                        response['status_code'] = 400
                        response['data'] = unknown_text
                        response['message'] = """I'm sorry, I can't understand you"""
                
        except Exception as e:
            print('error3: ',str(e))
            response['status_code'] = 400
            response['data'] = None
            response['message'] = str(e)

        return response
    
    def chat_hold(self):
        # print('=============HoldWorking========')
        response = {'status_code':200, "message":"successful","data":[]}
        getHoldTextTemp = [x for x in self.chatTemp if x['roomId']==self.token['chatid']][0]
        getHoldText = [x for x in getHoldTextTemp['responseList'].split('|') if x]
        getRoomIds = [x for x in self.roomDetails if x['roomId']==self.token['chatid']][0]
        # print('========CheckHoldValue==========')

        # print(self.chatTemp) 


        
        # print(getHoldTextTemp)
        # print('#===============================#')

        # Duration Text Check
        if 'duration' in list(getHoldTextTemp.keys()):
            for i,tempChange in enumerate(self.chatTemp):
                if 'duration' in list(tempChange.keys()):
                    getHoldDuration = [x for x in tempChange['duration'] if x == self.token['user']]
                    if len(getHoldDuration) == 1:
                        tempChange['duration'].clear()
                        tempChange['duration'].append(getHoldDuration[0])


        # Json Template
        temp_val = {"response_id": None,
                      "response_text": [
                        {
                          "name": None,
                          "action_type": "title",
                          "setValue": None
                        },
                      ],
                      "option": None,
                      "message_type":"patient_request",
                      "action": None,
                      "m_type": "patient_request",
                    }
        
        # setValue in json
        if 'hold' in getHoldText:
            getHoldText.remove('hold')
            temp_val['response_text'][0]['name'] = self.token['user']
            temp_val['option'] = getHoldText[0]
            

            if len([x for x in ['other','next','&'] if str(self.token['user']).lower().find(x)==-1]) == 3:
                self.obj.sideEffectAdd(data={'user_id':getRoomIds['userId'],'side_id':self.token['user']}, token=getRoomIds['token'])

            
            for setDictText in self.chatTemp:
                if setDictText['roomId']==self.token['chatid']:
                    setDictText['text'] = self.token['user']
                    setDictText['responseList'] = ""

            response['status_code'] = 200
            response['message'] = 'successful value change and set'
            response['data'] = temp_val
            getHoldTextTemp['status'] = "INACTIVE"
            
        elif 'holdPi1' in getHoldText:
            getHoldText.remove('holdPi1')
            temp_val['response_text'][0]['name'] = self.token['user']
            temp_val['option'] = getHoldText[0]
            
            for setDictText in self.chatTemp:
                if setDictText['roomId']==self.token['chatid']:
                    setDictText['patient_d1'] = self.token['user']
                    setDictText['responseList'] = ""

            response['status_code'] = 200
            response['message'] = 'successful value change and set'
            response['data'] = temp_val
            getHoldTextTemp['status'] = "INACTIVE"

        elif 'holdPi2' in getHoldText:
            getHoldText.remove('holdPi2')
            temp_val['response_text'][0]['name'] = self.token['user']
            temp_val['option'] = getHoldText[0]
            
            for setDictText in self.chatTemp:
                if setDictText['roomId']==self.token['chatid']:
                    setDictText['patient_d2'] = self.token['user']
                    setDictText['responseList'] = ""

            response['status_code'] = 200
            response['message'] = 'successful value change and set'
            response['data'] = temp_val
            getHoldTextTemp['status'] = "INACTIVE"

        elif 'duration' in list(getHoldTextTemp.keys()) and self.token['user'] in getHoldTextTemp['duration']:
            temp_val['response_text'][0]['name'] = self.token['user']
            getHoldTextTemp['text'] = str(getHoldTextTemp['text']).replace(' ','_')
            temp_val['option'] = f"&{getHoldTextTemp['text']}&"

            response['status_code'] = 200
            response['message'] = 'successful value change and set'
            response['data'] = temp_val
            getHoldTextTemp['status'] = "INACTIVE"

        else:
            response['status_code'] = 200
            response['message'] = 'change value error'
            response['data'] = None
        
        # chatTemp variable value delete            
        # for x,y in enumerate(self.chatTemp):
        #     if self.token['chatid'] in self.chatTemp[x].keys():
        #         del self.chatTemp[x]
                
        return response
    
    def chat_history(self, action=None, data={}, status="active"):
        response = {'status_code':200, "message":"successful","data":[]}
                
        # action=insert/update
        # if update data={"chatId":roomId, response:chatbot/user, text:id/text}
        # if insert data={'chatId':roomId,'name':getName,'cr_no':getCrNo,'userId':getId}
        # if update data={'chatId':roomId,response:status}, status=inactive
        
        
        if action == 'insert' and len(data) == 4:
            chatHist = {
                "chat_id":data['chatId'],
                "user_id":data['userId'],
                "cr_no":data['cr_no'],
                "name":data['name'],
                "data":[],
                "status":status
            }
            insertUserHistData = {"order":"insert","data":{"table":"tbl_UserChatHistory","condition":chatHist}}
            # insertUserHist = self.database(insertUserHistData)
            
        elif action == 'update':
            
            timestamp = str(dt.datetime.now())
            timestamp = timestamp[:timestamp.rfind('.')]
            
            selectUserHistData = {"order":"find","data":{"table":"tbl_UserChatHistory","condition":{"chat_id":data['chatId']}}}
            # selectUserHist = self.database(selectUserHistData)
            
            updateUserHistId = selectUserHist['data']['_id']
            updateUserHistData = selectUserHist['data']['data']
            updateUserHistStatus = selectUserHist['data']['status']
            
            # Update ChatHistory
            if data['response'] == 'chatbot':
                updateUserHistData.append({'response_id':data['text'],"timestamp":timestamp})
                PrepareData = {"old":{"_id":ObjectId(updateUserHistId)},"new":{"data":updateUserHistData}}
                updateUserHistData = {"order":"update","data":{"table":"tbl_UserChatHistory","condition":PrepareData}}
                # updateUserHist = self.database(updateUserHistData)
                
            # Update ChatHistory
            if data['response'] == 'user':
                updateUserHistData.append({'request_text': data['text'],"timestamp":timestamp})
                PrepareData = {"old":{"_id":ObjectId(updateUserHistId)},"new":{"data":updateUserHistData}}
                updateUserHistData = {"order":"update","data":{"table":"tbl_UserChatHistory","condition":PrepareData}}
                # updateUserHist = self.database(updateUserHistData)
                
            if data['response'] == 'status':
                PrepareData = {"old":{"_id":ObjectId(updateUserHistId)},"new":{"status":status}}
                updateUserHistData = {"order":"update","data":{"table":"tbl_UserChatHistory","condition":PrepareData}}
                # updateUserHist = self.database(updateUserHistData)
            
        
        return response



# {"password": true, "email_id": "letstalkpatient1@yopmail.com", "role_id": "3", "_id": "62d5026f38d369ea5f5893b8", "exp": 1659252106, "mobile": "7249999809"}
# obj = chatbotObj()