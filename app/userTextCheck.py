class text:
    def checkTxt(self,check):
        #check = "hola"
        text = ["hi","hello","good morning","good afternoon","good evening","morning","noon","evening","hola"]
        textCheck = {"status":False,"text":"","request_text":check}

        for txt in text:
            if txt.replace(" ","").lower() == check.replace(" ","").lower():
                textCheck["status"] = True
                textCheck["text"] = "&"+txt.replace(" ","")+"&"
                break
            else:
                textCheck["status"] = False
                textCheck["text"] = check

        return textCheck
