from app.db_config import database_connect_mongo
from app.ChatBotModels import chatbotObj
from app.db_chatHist import db_hist

import datetime as dt
import websockets
import asyncio
import jwt
import json
from io import StringIO



objChat = chatbotObj()
objdb_hist = db_hist()
io = StringIO()


# Server data
# PORT = 6040



# A set of connected ws clients
connected = set()


# def create_app(config_name):

async def create_app(websocket, path):
    print("A client just connected")
    # Store a copy of the connected client
    # connected.add(websocket)
    # Handle incoming messages
    try:
        async for message in websocket:
            # print("Received message from client: " + message)

            message = json.loads(message)
            dateTime = str(dt.datetime.now())
            dateTime = dateTime[:dateTime.rfind('.')]
            print('=============Chatbot=========')
            print(dateTime,message)
            print(type(message))
            if type(message)==dict:
                await asyncio.sleep(0.25)
                objChat.main(message)
                print('---------------------')
                # print(objChat.roomDetails)
                objResponse = objChat.response()

                if "chatid" in list(message.keys()):
                    data = {'chatId': message['chatid'], 'response': "chatbot", 'text':objResponse}
                    objdb_hist.chat_history(action="update", data=data, status="ACTIVE")

                objResponse['datetime'] = dateTime

                print(dateTime,objResponse)
                print('---------------------')
                print(len(objChat.roomDetails))
                # await asyncio.sleep(0.10)
                await websocket.send(json.dumps(objResponse))
                
                # await asyncio.sleep(5)
                # await websocket.send("its working")
            else:
                await websocket.send("str value")

            print('=============================')

            
            # Send a response to all connected clients except sender
            
            # await websocket.send(f"Someone said: {message}")
    # Handle disconnecting clients 
    # except Exception as e:
    except websockets.exceptions.ConnectionClosed as e:
        print("A client just disconnected")
        print("Error: ",str(e))
    finally:
        pass
        # connected.remove(websocket)

	# return app
