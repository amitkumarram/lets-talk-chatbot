
class PatientCondition:
    def check(self, data):
        self.data = data
        patient_d1 = self.data['patient_d1']
        patient_d1 = patient_d1[:patient_d1.find(')')]
        patient_d2 = self.data['patient_d2']
        patient_d2 = patient_d2[:patient_d2.find(')')]
        nextid = str(self.data['responseList']).replace('|','').replace('stop','')

        
        result = {'res1':'','res2':'Stop the TKI and visit opthalmologist/ report to OPD immediately'}
        if str(self.data['text']).lower() == 'nausea/vomitting':
            if str(patient_d1) == '1' and str(patient_d2) == 'a':
                result['res1'] = "Mild tox"
                result['res2'] = "Take small meals & eat more often. Please call OPD/ Emg number"
            elif str(patient_d1) in ['2','3','4'] and str(patient_d2) == 'a':
                result['res1'] = "Moderate tox"
                result['res2'] = "Stop the TKI & Visit OPD"
            elif str(patient_d1) in ['5','6'] and str(patient_d2) == 'a':
                result['res1'] = "Severe Tox"
                result['res2'] = "Stop the TKI &Visit Emergency local clinic ( if staying afar)"
            elif str(patient_d1) in ['1','2','3','4','5','6'] and str(patient_d2) == 'b':
                result['res1'] = "Severe Tox"
                result['res2'] = "Stop the TKI &Visit Emergency / local clinic ( if staying afar) immediately"
            else:
                pass
                # result['res1'] = ""
                # result['res2'] = ""
                
        elif str(self.data['text']).lower() == 'diarrhoea':
            if str(patient_d1) == '1' and str(patient_d2) == 'a':
                result['res1'] = "Mild tox"
                result['res2'] = "Hydrate yourself adequately. Take Redotil capsule 100mg stat. Please call OPD/ Emg number"
            elif str(patient_d1) in ['2'] and str(patient_d2) == 'a':
                result['res1'] = "Moderate tox"
                result['res2'] = "Stop the TKI & Visit OPD/Emergency"
            elif str(patient_d1) in ['3'] and str(patient_d2) == 'a':
                result['res1'] = "Borderline Severe Tox"
                result['res2'] = "Stop the TKI &Visit Emergency local clinic ( if staying afar)"
            elif str(patient_d1) in ['1','2','3'] and str(patient_d2) == 'b':
                result['res1'] = "Severe tox"
                result['res2'] = "Stop the TKI &Visit Emergency / local clinic ( if staying afar) immediately"
            elif str(patient_d1) in ['4'] and str(patient_d2) in ['b','a']:
                result['res1'] = "Severe tox"
                result['res2'] = "Stop the TKI &Visit Emergency / local clinic ( if staying afar) immediately"
            else:
                pass
                # result['res1'] = ""
                # result['res2'] = ""
                
        elif str(self.data['text']).lower() == 'constipation':
            if str(patient_d1) == '1' and str(patient_d2) == 'a':
                result['res1'] = "Mild tox"
                result['res2'] = """Use prescribed laxatives- begin with 20ml laxative syrup/ bisacodyl suppository before going to sleep. Please call OPD/ Emg number for further assistance"""
            elif str(patient_d1) in ['2'] and str(patient_d2) == 'a':
                result['res1'] = "Moderate tox"
                result['res2'] = "Stop the TKI & Visit OPD/Emergency"
            elif str(patient_d1) in ['3'] and str(patient_d2) == 'a':
                result['res1'] = "Severe Tox"
                result['res2'] = "Stop the TKI &Visit Emergency / local clinic ( if staying afar)"
            elif str(patient_d1) in ['1','2','3'] and str(patient_d2) == 'b':
                result['res1'] = "Severe tox"
                result['res2'] = "Stop the TKI &Visit Emergency / local clinic ( if staying afar) immediately"
            elif str(patient_d1) in ['4'] and str(patient_d2) in ['b','a']:
                result['res1'] = "Severe tox"
                result['res2'] = "Stop the TKI &Visit Emergency / local clinic ( if staying afar) immediately"
            else:
                pass
                # result['res1'] = ""
                # result['res2'] = ""
        
        elif str(self.data['text']).lower() == 'bradycardia':
            if str(patient_d1) == '1' and str(patient_d2) == 'a':
                result['res1'] = "Mild tox"
                result['res2'] = """Check for heart rate hourly over at least 6 hours . If persistenly below 60, Please call OPD/ Emg number for further assistance"""
            elif str(patient_d1) in ['1'] and str(patient_d2) in ['b','c']:
                result['res1'] = "Needs evaluation"
                result['res2'] = "Stop the TKI & Visit OPD/Emergency"
            elif str(patient_d1) in ['2'] and str(patient_d2) in ['a','c']:# No data found
                result['res1'] = "Needs urgent evaluation"
                result['res2'] = ""
            elif str(patient_d1) in ['3'] and str(patient_d2) in ['a','c']:
                result['res1'] = "Needs immediate cardiology consultation / admission"
                result['res2'] = """Stop the TKI &Visit Cardiac Centre/ Emergency/ local clinic ( if staying afar) with cardiology facility"""
            else:
                pass
                # result['res1'] = ""
                # result['res2'] = ""
                
        elif str(self.data['text']).lower() == 'dysphagia':
            if str(patient_d1) == '1' and str(patient_d2) == 'a':
                result['res1'] = "Mild tox"
                result['res2'] = "Take semisolid/liquid diet in smaller quantities and more frequently"
            elif str(patient_d1) == '1' and str(patient_d2) == 'b':
                result['res1'] = "Moderate Tox"
                result['res2'] = "Stop the TKI & Visit OPD/Emergency"
            elif str(patient_d1) == '2' and str(patient_d2) == 'a':
                result['res1'] = "Moderate Tox"
                result['res2'] = "Stop the TKI & Visit OPD/Emergency"
            elif str(patient_d1) == '2' and str(patient_d2) == 'b':
                result['res1'] = "Severe tox"
                result['res2'] = "Stop the TKI & Visit OPD/Emergency"
            elif str(patient_d1) == '3' and str(patient_d2) in ['a','b']:
                result['res1'] = "Severe Tox"
                result['res2'] = """Stop the TKI &Visit Emergency/ local clinic ( if staying afar) for IV fluids, Ryle's tube insertion, TPN, etc and further evaluation"""
            elif str(patient_d1) == '4' and str(patient_d2) in ['a','b']:
                result['res1'] = "Severe Tox"
                result['res2'] = """Stop the TKI &Visit Emergency/ local clinic ( if staying afar) immediately for IV fluids, Ryle's tube insertion,TPN, etc and further evaluation"""
            else:
                pass
                # result['res1'] = ""
                # result['res2'] = ""
                
        elif str(self.data['text']).lower() == 'vision':
            if str(patient_d1) == '1' and str(patient_d2) == 'a':
                result['res1'] = "Mild tox"
                result['res2'] = "Stop the TKI and visit opthalmologist/ report to OPD immediately"
            elif str(patient_d1) == '1' and str(patient_d2) == 'b':
                result['res1'] = "Moderate Tox"
                result['res2'] = "Stop the TKI and visit opthalmologist/ report to OPD immediately"
            elif str(patient_d1) == '2' and str(patient_d2) == 'a':
                result['res1'] = "Moderate Tox"
                result['res2'] = "Stop the TKI and visit opthalmologist/ report to OPD immediately"
            elif str(patient_d1) == '2' and str(patient_d2) == 'b':
                result['res1'] = "Severe tox"
                result['res2'] = "Stop the TKI and visit opthalmologist/ report to OPD immediately"
            elif str(patient_d1) == '3' and str(patient_d2) in ['a','b']:
                result['res1'] = "Severe Tox"
                result['res2'] = "Stop the TKI and visit opthalmologist/ report to OPD immediately"
            elif str(patient_d1) == '4' and str(patient_d2) in ['a','b']:
                result['res1'] = "Severe Tox"
                result['res2'] = "Stop the TKI and visit opthalmologist/ report to OPD immediately"
            else:
                pass
                # result['res1'] = ""
                # result['res2'] = ""
        
        elif str(self.data['text']).lower() == 'increased_weight':
            if str(patient_d1) == '1' and str(patient_d2) == 'a':
                result['res1'] = "Mild tox"
                result['res2'] = "Consult dietician/ nutritionist"
            elif str(patient_d1) == '1' and str(patient_d2) == 'b':
                result['res1'] = "Moderate Tox"
                result['res2'] = "Consult dietician/ nutritionist, Stop the TKI & Visit OPD"
            elif str(patient_d1) == '2' and str(patient_d2) == 'a':
                result['res1'] = "Moderate Tox"
                result['res2'] = "Stop the TKI & Visit OPD. "
            elif str(patient_d1) == '2' and str(patient_d2) == 'b':
                result['res1'] = "Severe tox"
                result['res2'] = "Stop the TKI & Visit OPD. "
            elif str(patient_d1) == '3' and str(patient_d2) in ['a','b']:
                result['res1'] = "Severe Tox"
                result['res2'] = "Stop the TKI & Visit OPD/ Emergency. "
            else:
                pass
                # result['res1'] = ""
                # result['res2'] = ""
                
        elif str(self.data['text']).lower() == 'ecg-_qt_prolonged':
            if str(patient_d1) == '1' and str(patient_d2) == 'a':
                result['res1'] = "Mild tox"
                result['res2'] = """Stop TKI and undergo an ECG at home/ nearby hospital/ Emergency. You might need cardiology consultation."""
            elif str(patient_d1) == '1' and str(patient_d2) == 'b':
                result['res1'] = "Moderate Tox"
                result['res2'] = """Stop TKI and undergo an ECG at home/ nearby hospital/ Emergency.You need cardiology consultation."""
            elif str(patient_d1) == '2' and str(patient_d2) == 'a':
                result['res1'] = "Moderate Tox"
                result['res2'] = """Stop TKI and undergo an ECG at home/ nearby hospital/ Emergency.You need cardiology consultation."""
            elif str(patient_d1) == '2' and str(patient_d2) == 'b':
                result['res1'] = "Severe tox"
                result['res2'] = """Stop TKI and undergo an ECG at home/ nearby hospital/ Emergency.You need cardiology consultation."""
            elif str(patient_d1) == '3' and str(patient_d2) in ['a','b']:
                result['res1'] = "Severe Tox"
                result['res2'] = """Stop TKI and rush to Emergency or nearby hospital (preferably with Cardiology expertise) if staying afar!  You need to undergo an ECG immediately and  then cardiology consultation"""
            else:
                pass
                # result['res1'] = ""
                # result['res2'] = ""
                
        elif str(self.data['text']).lower() == 'headache':
            if str(patient_d1) == '1' and str(patient_d2) == 'a':
                result['res1'] = "Mild tox"
                result['res2'] = "Take semisolid/liquid diet in smaller quantities and more frequently"
            elif str(patient_d1) == '1' and str(patient_d2) == 'b':
                result['res1'] = "Moderate Tox"
                result['res2'] = "Stop the TKI & Visit OPD/Emergency"
            elif str(patient_d1) == '2' and str(patient_d2) == 'a':
                result['res1'] = "Moderate Tox"
                result['res2'] = " Stop the TKI & Visit OPD/Emergency"
            elif str(patient_d1) == '2' and str(patient_d2) == 'b':
                result['res1'] = "Severe tox"
                result['res2'] = " Stop the TKI & Visit OPD/Emergency"
            elif str(patient_d1) == '3' and str(patient_d2) in ['a','b']:
                result['res1'] = "Severe Tox"
                result['res2'] = """Stop the TKI &Visit Emergency/ local clinic ( if staying afar) for IV fluids, Ryle's tube insertion, TPN, etc and further evaluation"""
            elif str(patient_d1) == '4' and str(patient_d2) in ['a','b']:
                result['res1'] = "Severe Tox"
                result['res2'] = """Stop the TKI &Visit Emergency/ local clinic ( if staying afar) immediately for IV fluids, Ryle's tube insertion,TPN, etc and further evaluation"""
            else:
                
                result['res1'] = ""
                result['res2'] = "Stop the TKI and visit opthalmologist/ report to OPD immediately"
                
        
        if str(str(result['res2']).lower()).find('stop') != -1:
            result['res3'] = "stop" 
        else:
            result['res3'] = nextid
                
        
        return result
